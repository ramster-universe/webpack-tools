# 2.0.0
- Vastly improved structure and tests, including code coverage reports.
- Improved docs, readme and repo badges.

# 2.0.0-rc.0
- Initial version. Contains the webpackBuild and webpackDevserver functionality from ramster@1.x, improved and exported into a separate module.
- Added a CLI and some docs in the readme.md file.
