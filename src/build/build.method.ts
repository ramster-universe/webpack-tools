import * as webpack from 'webpack'

/**
 * Builds the webpack bundle for the provided config profile.
 * @param config - The config to use and pass to webpack.
 * @returns A promise that resolves with true if the execution is successful.
 */
export async function build(config: webpack.Configuration): Promise<boolean> {
	return await (new Promise((resolve, reject) => {
		webpack(config, (err, stats) => {
			if (err || stats.hasErrors()) {
				reject(err || stats.toJson('errors-only'))
			}
			resolve(true)
		})
	}))
}
