import {build} from '../../dist'
import * as fs from 'fs-extra'
import * as path from 'path'
import {strict as assert} from 'assert'

const
	basePath = path.join(__dirname, '../../test')

describe('build', function() {
	it('should execute successfully and throw an error if there are internal errors', function() {
		this.timeout(25000)
		return (async function() {
			let didThrowAnError = true
			try {
				await build((await import(path.join(basePath, 'webpackConfigWithInternalErrors.js'))).webpackConfig)
			}
			catch(e) {
				console.error(e)
				didThrowAnError = true
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it('should execute successfully and throw an error if there are problems with the build', function() {
		this.timeout(25000)
		return (async function() {
			let didThrowAnError = true
			try {
				await build((await import(path.join(basePath, 'webpackConfigWithSrcErrors.js'))).webpackConfig)
			}
			catch(e) {
				console.error(e)
				didThrowAnError = true
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it('should execute successfully, build the bundle and generate a main.js file if a valid config is provided', function() {
		this.timeout(25000)
		return (async function() {
			await build((await import(path.join(basePath, 'webpackConfig.js'))).webpackConfig)
			let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
			assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
		})()
	})
	after(function() {
		return (async function() {
			await fs.remove(path.join(basePath, 'dist/main.js'))
			await fs.remove(path.join(__dirname, '../dist/test'))
		})()
	})
})
