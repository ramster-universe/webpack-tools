import {CustomError} from '@ramster/general-tools'
import {build, injectPluginsInConfig} from '../'

/**
 * Loads the file at the provided path and builds the webpack bundle for it. The method expects the file to contain two objects:
 * * profileWebpackPlugins - (optional) - An object, whose keys are the names of the config profiles and its values are arrays of webpack plugins
 * * webpackConfig - (required) An object of type webpack.Configuration
 * @param configFilePath (required) - The path to the config file to use and pass to webpack.
 * @param pluginsProfileName (optional) - If provided, adds the list of plugins under the corresponding key in the config to the plugin array. For example, "production" would add "productionPlugins" from the config to "plugins" in the config.
 * @returns A promise that resolves with true if the execution is successful.
 */
export async function buildFromFile(configFilePath: string, pluginsProfileName?: string): Promise<boolean> {
	console.log('[ramster-webpack build info]: Starting script...')
	let configData = await import(configFilePath)
	console.log('[ramster-webpack build info]: Config file loaded.')
	if (!configData.webpackConfig) {
		throw new CustomError('The provided file does not contain a webpackConfig object.')
	}
	if (pluginsProfileName && configData.profileWebpackPlugins) {
		configData.webpackConfig = injectPluginsInConfig(configData.webpackConfig, configData.profileWebpackPlugins, pluginsProfileName)
	}
	console.log('[ramster-webpack build info]: Starting build...')
	await build(configData.webpackConfig)
	console.log(`[ramster-webpack build info]: Build for config file ${configFilePath} started successfully.`)
	return true
}
