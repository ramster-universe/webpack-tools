import {buildFromFile} from '../../dist'
import * as fs from 'fs-extra'
import * as path from 'path'
import {strict as assert} from 'assert'

const
	basePath = path.join(__dirname, '../../test')

describe('buildFromFile', function() {
	before(function() {
		return (async function() {
			await fs.remove(path.join(basePath, 'dist/main.js'))
		})()
	})
	it(`should throw an error with the correct message if a path to an invalid config file that's missing a webpackConfig object is provided`, function() {
		return (async function() {
			let didThrowAnError = true,
				messageShouldBe = `The provided file does not contain a webpackConfig object.`
			try {
				await buildFromFile(path.join(basePath, 'invalidWebpackConfig.js'))
			} catch(e) {
				assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected CustomError`)
				assert.strictEqual(e.message, messageShouldBe, `bad value ${e.message} for e.message, expected ${messageShouldBe}`)
				didThrowAnError = true
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it('should execute successfully, build the bundle and generate a main.js file if a path to a valid config file is provided and no pluginsProfileName is provided', function() {
		this.timeout(25000)
		return (async function() {
			await buildFromFile(path.join(basePath, 'webpackConfig.js'))
			let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
			assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
			await fs.remove(path.join(basePath, 'dist/main.js'))
		})()
	})
	it('should execute successfully, build the bundle and generate a main.js file if a path to a valid config file is provided and pluginsProfileName is provided', function() {
		this.timeout(25000)
		return (async function() {
			await buildFromFile(path.join(basePath, 'webpackConfig.js'), 'production')
			let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
			assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
		})()
	})
	after(function() {
		return (async function() {
			await fs.remove(path.join(basePath, 'dist/main.js'))
			await fs.remove(path.join(__dirname, '../dist/test'))
		})()
	})
})
