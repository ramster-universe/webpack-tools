import {strict as assert} from 'assert'
import {exec, spawn} from 'child_process'
import * as fs from 'fs-extra'
import * as path from 'path'
import * as request from 'request-promise'

const
	basePath = path.join(__dirname, '../../test'),
	nycPath = path.join(__dirname, '../../node_modules/.bin/nyc'),
	checkDevserver = () => new Promise((resolve, reject) => {
		request('http://127.0.0.1:9999/dist/main.js').then(
			() => resolve(true),
			(err) => reject(err)
		)
	}),
	distPath = path.join(__dirname, '../../dist'),
	execPromise = (command: string, options?: {skipStdErr?: boolean}) => new Promise((resolve, reject) => {
		exec(command, (err, stdout, stderr) => {
			const {skipStdErr} = options || {}
			console.log(`[${command} err]`, err)
			console.log(`[${command} stderr]`, stderr)
			console.log(`[${command} stdout]`, stdout)
			if (err) {
				reject(err)
			}
			if (stderr && !skipStdErr) {
				reject(new Error(stderr))
			}
			let result = ''
			if (stdout && stdout.length) {
				result = stdout.substr(0, stdout.length - 1)
			}
			resolve(result)
		})
	}),
	killDevserver = () => new Promise((resolve, reject) => {
		exec('fuser -k 9999/tcp', (err, stdout, stderr) => {
			console.log('[killDevserver err]', err)
			console.log('[killDevserver stderr]', stderr)
			console.log('[killDevserver stderr]', stdout)
			if (err) {
				reject(err)
			}
			resolve(true)
		})
	}),
	spawnPromise = (args: string[], options?: {resolvePromiseOnStdOutString?: string}) => (async function() {
		const {resolvePromiseOnStdOutString} = options || {}
		await new Promise((resolve, reject) => {
			let proc = spawn('node', args),
				stdout = '',
				stderr = ''
			proc.stdout.on('data', (data) => {
				let stringData = data.toString()
				console.log(`[${args[0]} stdout]:`, stringData)
				stdout+= stringData
				if (resolvePromiseOnStdOutString && stringData && (stringData.indexOf(resolvePromiseOnStdOutString) !== -1)) {
					resolve(stdout)
				}
			})
			proc.stderr.on('data', (data) => {
				let stringData = data.toString()
				console.log(`[${args[0]} stderr]:`, stringData)
				stderr+= stringData
			})
			proc.on('close', (code) => {
				console.log(`[${args[0]} exit code]:`, code)
				if (code === 0) {
					resolve(stdout)
				}
				reject(new Error(stderr))
			})
		})
		return true
	})()


describe('cli', function() {
	describe('no valid arguments provided', function() {
		it('should execute successfully and return the correct message if no arguments are provided', function() {
			this.timeout(40000)
			return (async function() {
				let result = await execPromise(`${nycPath} --silent node ${path.join(distPath, '/cli/cli.js')}`),
					resultShouldBe = 'No arguments provided. Run the command with the -h flag for help.'
				assert.strictEqual(result, resultShouldBe, `bad value ${result} for the output, expected ${resultShouldBe}`)
			})()
		})
	})


	describe('help arguments provided', function() {
		it('should execute successfully and return the correct message if the -h argument is provided', function() {
			this.timeout(40000)
			return (async function() {
				let result = (await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -h`)) as any
				assert.strictEqual(result.indexOf('Usage: '), 0, `bad value ${result} for the output, expected it to start with 'Usage: '`)
			})()
		})
	})


	describe('configFilePath provided', function() {
		describe('no runmode argument provided', function() {
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the -c argument, but not runmode flag is provided', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'Please provide a runmode flag - "-b" ("--build") or "-w" ("--watch").'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -c ${path.join(basePath, 'webpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the --configFilePath argument, but not runmode flag is provided', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'Please provide a runmode flag - "-b" ("--build") or "-w" ("--watch").'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --configFilePath=${path.join(basePath, 'webpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
		})


		describe(`the provided number of configFileNames doesn't match the provided number of pluginProfilePaths`, function() {
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the -c argument, pluginProfileNames are provided using the -p argument, but their count does not match the provided configFilePath argument count', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'The number of configFilePaths must match the number of pluginProfileNames.'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -c ${path.join(basePath, 'webpackConfig.js')} -c 1234 -p production -p staging -p local`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the -c argument, pluginProfileNames are provided using the --pluginProfileName argument, but their count does not match the provided configFilePath argument count', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'The number of configFilePaths must match the number of pluginProfileNames.'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -c ${path.join(basePath, 'webpackConfig.js')} -c 1234 --pluginProfileName=production --pluginProfileName=staging --pluginProfileName=local`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the --configFilePath argument, pluginProfileNames are provided using the -p argument, but their count does not match the provided configFilePath argument count', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'The number of configFilePaths must match the number of pluginProfileNames.'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --configFilePath=${path.join(basePath, 'webpackConfig.js')} --configFilePath=1234 -p production -p staging -p local`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
			it('should execute successfully, exit with code 1 and return the correct message if a valid configFilePath is provided using the --configFilePath argument, pluginProfileNames are provided using the --pluginProfileName argument, but their count does not match the provided configFilePath argument count', function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = false,
						errorShouldBe = 'The number of configFilePaths must match the number of pluginProfileNames.'
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --configFilePath=${path.join(basePath, 'webpackConfig.js')} --configFilePath=1234 --pluginProfileName=production --pluginProfileName=staging --pluginProfileName=local`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(errorShouldBe), -1, `bad value ${e.message} for the output, expected it to contain ${errorShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `no error thrown`)
				})()
			})
		})


		describe('build', function() {
			before(function() {
				return (async function() {
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})

			it(`should throw an error with the correct message if a -b runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the -c argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b -c ${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is provided using the -c argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b -c ${path.join(basePath, 'webpackConfig.js')}`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is using the -c argument provided and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b -c ${path.join(basePath, 'webpackConfig.js')} -p production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is using the -c argument provided and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b -c ${path.join(basePath, 'webpackConfig.js')} --pluginProfileName=production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it(`should throw an error with the correct message if a -b runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the --configFilePath argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b --configFilePath=${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is provided using the --configFilePath and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b --configFilePath=${path.join(basePath, 'webpackConfig.js')}`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is using the --configFilePath argument provided and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b --configFilePath=${path.join(basePath, 'webpackConfig.js')} -p production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a -b runmode argument is provided, a path to a valid config file is using the --configFilePath argument provided and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -b --configFilePath=${path.join(basePath, 'webpackConfig.js')} --pluginProfileName=production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})

			it(`should throw an error with the correct message if a --build runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the -c argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build -c ${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is provided using the -c argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build -c ${path.join(basePath, 'webpackConfig.js')}`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is using the -c argument provided and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build -c ${path.join(basePath, 'webpackConfig.js')} -p production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is using the -c argument provided and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build -c ${path.join(basePath, 'webpackConfig.js')} --pluginProfileName=production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it(`should throw an error with the correct message if a --build runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the --configFilePath argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build --configFilePath=${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is provided using the --configFilePath and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build --configFilePath=${path.join(basePath, 'webpackConfig.js')}`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is using the --configFilePath argument provided and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build --configFilePath=${path.join(basePath, 'webpackConfig.js')} -p production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})
			it('should execute successfully, build the bundle and generate a main.js file if a --build runmode argument is provided, a path to a valid config file is using the --configFilePath argument provided and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --build --configFilePath=${path.join(basePath, 'webpackConfig.js')} --pluginProfileName=production`, {skipStdErr: true})
					let isFile = (await fs.lstat(path.join(basePath, 'dist/main.js'))).isFile()
					assert.strictEqual(isFile, true, `bad value ${isFile} for isFile, expected true`)
					await fs.remove(path.join(basePath, 'dist/main.js'))
				})()
			})

			after(function() {
				return (async function() {
					await fs.remove(path.join(basePath, 'dist/main.js'))
					await fs.remove(path.join(__dirname, '../dist/test'))
				})()
			})
		})


		describe('watch', function() {
			it(`should throw an error with the correct message if a -w runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the -c argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -w -c ${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the -c argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', '-c', path.join(basePath, 'webpackConfig.js')], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', '-c', path.join(basePath, 'webpackConfig.js'), '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', '-c', path.join(basePath, 'webpackConfig.js'), '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', '-c', path.join(basePath, 'webpackConfig.js'), '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', '-c', path.join(basePath, 'webpackConfig.js'), '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it(`should throw an error with the correct message if a -w runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the --configFilePath argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} -w --configFilePath=${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a -w runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '-w', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})

			it(`should throw an error with the correct message if a --watch runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the -c argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --watch -c ${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the -c argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', '-c', path.join(basePath, 'webpackConfig.js')], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', '-c', path.join(basePath, 'webpackConfig.js'), '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', '-c', path.join(basePath, 'webpackConfig.js'), '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', '-c', path.join(basePath, 'webpackConfig.js'), '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the -c argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', '-c', path.join(basePath, 'webpackConfig.js'), '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it(`should throw an error with the correct message if a --watch runmode argument is provided, a path to an invalid config file that's missing a webpackConfig object is provided using the --configFilePath argument`, function() {
				this.timeout(40000)
				return (async function() {
					let didThrowAnError = true,
						messageShouldBe = `The provided file does not contain a webpackConfig object.`
					try {
						await execPromise(`${nycPath} --silent node ${path.join(distPath, 'cli/cli.js')} --watch --configFilePath=${path.join(basePath, 'invalidWebpackConfig.js')}`)
					} catch(e) {
						assert.notStrictEqual(e.message.indexOf(messageShouldBe), -1, `bad value ${e.message} for e.message, expected it to contain ${messageShouldBe}`)
						didThrowAnError = true
					}
					assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and no pluginsProfileName is provided', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the -p argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '-p', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
			it('should execute successfully and start a devserver if a --watch runmode argument is provided, a path to a valid config file is provided using the --configFilePath argument and pluginsProfileName is provided using the --pluginProfileName argument', function() {
				this.timeout(40000)
				return (async function() {
					await spawnPromise([path.join(distPath, 'cli/cli.js'), '--watch', `--configFilePath=${path.join(basePath, 'webpackConfig.js')}`, '--pluginProfileName', 'production'], {resolvePromiseOnStdOutString: 'Compiled'})
					let isRunning = await checkDevserver()
					assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
					await killDevserver()
				})()
			})
		})
	})

	after(function() {
		this.timeout(11000)
		setTimeout(() => process.exit(0), 10000)
	})
})
