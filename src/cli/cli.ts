#!/usr/bin/node

import {argv} from 'yargs'
import {buildFromFile, watchFromFile} from '../'

const
	configFilePath = argv.c || argv.configFilePath,
	pluginProfileName = argv.p || argv.pluginProfileName

if (configFilePath) {
	(async function() {
		let configFilePaths = configFilePath instanceof Array ? configFilePath : [configFilePath],
			pluginProfileNames = []
		if (pluginProfileName instanceof Array) {
			pluginProfileNames = pluginProfileName
		}
		else if (pluginProfileName) {
			pluginProfileNames = [pluginProfileName]
		}
		if ((configFilePaths.length > 1) && (pluginProfileNames.length > 1) && (configFilePaths.length !== pluginProfileNames.length)) {
			throw new Error('The number of configFilePaths must match the number of pluginProfileNames.')
		}
		if (argv.b || argv.build) {
			for (const i in configFilePaths) {
				await buildFromFile(configFilePaths[i], pluginProfileNames[i])
			}
			return {exitProcess: true}
		}
		if (argv.w || argv.watch) {
			for (const i in configFilePaths) {
				await watchFromFile(configFilePaths[i], pluginProfileNames[i])
			}
			return {exitProcess: false}
		}
		throw new Error('Please provide a runmode flag - "-b" ("--build") or "-w" ("--watch").')
	})().then(
		(data: {exitProcess: boolean}) => {
			console.log(`[ramster-webpack info]: Script completed successfully.`)
			if (data.exitProcess) {
				process.exit(0)
			}
		},
		(err) => {
			console.error(err)
			process.exit(1)
		}
	)
}
else {
	if (argv.h || argv.help) {
		console.log(
			`Usage: ramster-webpack [options]\n` +
			`       ramster-webpack -c config/webpack.js -p production\n` +
			`       ramster-webpack --configFilePath=config/webpack.js --pluginProfileName=production\n` +
			`\n` +
			`Options:\n` +
			`  -b, --build                     signals the script to build the bundles for the provided config files and exit\n` +
			`  -w, --watch                     signals the script to build and watch the bundles for the provided config files by running a devserver\n` +
			`  -c, --configFilePath            required; specifies the config file to use for the run; if provided multiple times, multiple files will be built\n` +
			`  -p, --pluginProfileName         optional; specifies the plugins profile for the config file to use for the run; if provided multiple times, each one will be matched to a single --configFilePath in the order provided\n` +
			`  -h, --help                      display this menu\n`
		)
	} else {
		console.log('No arguments provided. Run the command with the -h flag for help.')
	}
	process.exit(0)
}
