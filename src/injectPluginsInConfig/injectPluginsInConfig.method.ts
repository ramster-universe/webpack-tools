import * as Immutable from 'immutable'
import * as webpack from 'webpack'

/**
 * Takes a webpack config and injects plugins into its "plugins" array. If the object doesn't have a "plugins" array, it will be created automatically.
 * @param config (required) - The webpack config to inject the plugins in.
 * @param profilePlugins (required) - The object containing the config-profile-specific plugin arrays.
 * @param configProfileName (required) - The name of the config profile to inject the plugins for.
 * @returns The config with the injected plugins.
 */
export function injectPluginsInConfig(
	config: webpack.Configuration,
	profilePlugins: {[configProfileName: string]: webpack.Plugin[]},
	configProfileName: string
): webpack.Configuration {
	let immutableConfig = Immutable.fromJS(config)
	if (profilePlugins[configProfileName]) {
		if (!immutableConfig.get('plugins')) {
			immutableConfig = immutableConfig.set('plugins', [])
		}
		immutableConfig = immutableConfig.set('plugins', immutableConfig.get('plugins').concat(profilePlugins[configProfileName]))
	}
	return immutableConfig.toJS() as webpack.Configuration
}
