import {injectPluginsInConfig} from '../../dist'
import {strict as assert} from 'assert'

describe('injectPluginsInConfig', function() {
	it('should execute successfully, do not modify the config and return an independent clone of the provided config if no plugin profile with the provided name exists in the provided profilePlugins objects', function() {
		let config = {test1: {test2: 3}} as any,
			newConfig = injectPluginsInConfig(config, {}, 'production') as any
		assert.strictEqual(newConfig.test1.test2, 3, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 3`)
		newConfig.test1.test2 = 5
		assert.strictEqual(config.test1.test2, 3, `bad value ${config.test1.test2} for config.test1.test2, expected 3`)
		assert.strictEqual(newConfig.test1.test2, 5, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 5`)
	})
	it(`should execute successfully, do not modify the config and return an independent clone of the provided config with the plugins injected if the original config doesn't have a plugins Array, the plugin profile with the provided name exists in the provided profilePlugins objects`, function() {
		let config = {test1: {test2: 3}} as any,
			newConfig = injectPluginsInConfig(config, {staging: [{bar: 'foo'} as any], production: [{foo: 'bar'} as any]}, 'production') as any
		assert.strictEqual(newConfig.test1.test2, 3, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 3`)
		newConfig.test1.test2 = 5
		assert.strictEqual(config.test1.test2, 3, `bad value ${config.test1.test2} for config.test1.test2, expected 3`)
		assert.strictEqual(newConfig.test1.test2, 5, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 5`)
		assert(typeof config.plugins === 'undefined', 'the original config was mutated with the new plugins array')
		assert(newConfig.plugins instanceof Array, 'the new config did not get injected with the new plugins array')
		assert.strictEqual(newConfig.plugins.length, 1, `bad value ${newConfig.plugins.length} for newConfig.plugins.length, expected 1`)
		assert.strictEqual(newConfig.plugins[0].foo, 'bar', `bad value ${newConfig.plugins[0].foo} for newConfig.plugins[0].foo, expected 'bar'`)
	})
	it(`should execute successfully, do not modify the config and return an independent clone of the provided config with the plugins injected if the original config has an empty plugins Array, the plugin profile with the provided name exists in the provided profilePlugins objects`, function() {
		let config = {plugins: [], test1: {test2: 3}} as any,
			newConfig = injectPluginsInConfig(config, {staging: [{bar: 'foo'} as any], production: [{foo: 'bar'} as any]}, 'production') as any
		assert.strictEqual(newConfig.test1.test2, 3, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 3`)
		newConfig.test1.test2 = 5
		assert.strictEqual(config.test1.test2, 3, `bad value ${config.test1.test2} for config.test1.test2, expected 3`)
		assert.strictEqual(newConfig.test1.test2, 5, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 5`)
		assert.strictEqual(config.plugins.length, 0, `bad value ${config.plugins.length} for config.plugins.length, expected 0`)
		assert(newConfig.plugins instanceof Array, 'the new config did not get injected with the new plugins array')
		assert.strictEqual(newConfig.plugins.length, 1, `bad value ${newConfig.plugins.length} for newConfig.plugins.length, expected 1`)
		assert.strictEqual(newConfig.plugins[0].foo, 'bar', `bad value ${newConfig.plugins[0].foo} for newConfig.plugins[0].foo, expected 'bar'`)
	})
	it(`should execute successfully, do not modify the config and return an independent clone of the provided config with the plugins injected if the original config has a non-empty plugins Array, the plugin profile with the provided name exists in the provided profilePlugins objects`, function() {
		let config = {plugins: [{bar: 'foo'}], test1: {test2: 3}} as any,
			newConfig = injectPluginsInConfig(config, {staging: [{bar: 'foo'} as any], production: [{foo: 'bar'} as any]}, 'production') as any
		assert.strictEqual(newConfig.test1.test2, 3, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 3`)
		newConfig.test1.test2 = 5
		assert.strictEqual(config.test1.test2, 3, `bad value ${config.test1.test2} for config.test1.test2, expected 3`)
		assert.strictEqual(newConfig.test1.test2, 5, `bad value ${newConfig.test1.test2} for newConfig.test1.test2, expected 5`)
		assert.strictEqual(config.plugins.length, 1, `bad value ${config.plugins.length} for config.plugins.length, expected 1`)
		assert(newConfig.plugins instanceof Array, 'the new config did not get injected with the new plugins array')
		assert.strictEqual(newConfig.plugins.length, 2, `bad value ${newConfig.plugins.length} for newConfig.plugins.length, expected 2`)
		assert.strictEqual(newConfig.plugins[0].bar, 'foo', `bad value ${newConfig.plugins[0].bar} for newConfig.plugins[0].bar, expected 'foo'`)
		assert.strictEqual(newConfig.plugins[1].foo, 'bar', `bad value ${newConfig.plugins[1].foo} for newConfig.plugins[1].foo, expected 'bar'`)
	})
})
