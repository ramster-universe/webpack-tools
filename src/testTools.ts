import {Server} from 'http'
import * as request from 'request-promise'


export function checkDevserver(timeout: number): Promise<boolean> {
	return (async function() {
		const intervalTime = 100
		for (let i = 0; i <= timeout; i += intervalTime) {
			let isRunning = false
			try {
				isRunning = await new Promise((resolve, reject) => {
					request('http://127.0.0.1:9999/dist/main.js').then(
						() => resolve(true),
						(err: any) => reject(err)
					)
				})
				return isRunning
			}
			catch(e) {
				await new Promise((resolve) => {
					setTimeout(() => resolve(true), intervalTime)
				})
			}
		}
		return false
	})()
}

export function closeDevserver(devServer: Server): Promise<boolean> {
	return new Promise((resolve, reject) => {
		devServer.close((err) => {
			if (err) {
				reject(err)
			}
			resolve(true)
		})
	})
}
