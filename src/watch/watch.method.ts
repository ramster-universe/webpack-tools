import {Server} from 'http'
import * as webpack from 'webpack'
import * as WebpackDevServer from 'webpack-dev-server'

/**
 * Sets up a webpack devserver for the provided config profile.
 * @param webpackConfig (required) - The config to use and pass to webpack.
 * @param devserverConfig (required) - The config, containing the hostName and port to use when starting the devserver.
 * @returns A promise that resolves with true if the execution is successful.
 */
export async function watch(
	webpackConfig: webpack.Configuration,
	devserverConfig: {hostName: string, port: number}
): Promise<Server> {
	return await (new Promise((resolve, reject) => {
		const devServer = new WebpackDevServer(
			webpack(webpackConfig), {
				publicPath: webpackConfig.output ? webpackConfig.output.publicPath : '',
				historyApiFallback: true,
				stats: {
					colors: true
				}
			}
		).listen(devserverConfig.port, devserverConfig.hostName, function (err) {
			// console.log('==> err:', err)
			if (err) {
				reject(err)
			}
			resolve(devServer)
		})
	}))
}
