import {checkDevserver, closeDevserver} from '../testTools'
import * as path from 'path'
import {strict as assert} from 'assert'
import {watch} from '../../dist'

const
	basePath = path.join(__dirname, '../../test')

describe('watch', function() {
	it(`should throw an error if there is an internal error`, function() {
		this.timeout(40000)
		return (async function() {
			let didThrowAnError = true,
				devServer = null
			try {
				devServer = await watch(
					(await import(path.join(basePath, 'webpackConfigWithInternalErrors.js'))).webpackConfig,
					{hostName: '127.0.0.1', port: 9999}
				)
			} catch(e) {
				didThrowAnError = true
			}
			if (devServer) {
				await closeDevserver(devServer)
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it(`should throw an error if there is a problem with the build`, function() {
		this.timeout(40000)
		return (async function() {
			let didThrowAnError = true,
				devServer = null
			try {
				devServer = await watch(
					(await import(path.join(basePath, 'webpackConfigWithSrcErrors.js'))).webpackConfig,
					{hostName: '127.0.0.1', port: 9999}
				)
			} catch(e) {
				didThrowAnError = true
			}
			if (devServer) {
				await closeDevserver(devServer)
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it('should execute successfully and start a devserver for the bundle if a valid config is provided', function() {
		this.timeout(40000)
		return (async function() {
			const devServer = await watch(
				(await import(path.join(basePath, 'webpackConfig.js'))).webpackConfig,
				{hostName: '127.0.0.1', port: 9999}
			)
			devServer.on(
				'error',
				(err) => {
					throw err
				}
			)
			let isRunning = await checkDevserver(19900)
			await closeDevserver(devServer)
			assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
		})()
	})
})
