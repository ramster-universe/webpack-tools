import {CustomError} from '@ramster/general-tools'
import {injectPluginsInConfig, watch} from '../'
import {Server} from 'http'

/**
 * Loads the file at the provided path and sets up a webpack devserver for it. The method expects the file to contain three objects:
 * * devserverConfig - (required) - An object of type {hostName: string, port: number}
 * * profileWebpackPlugins - (optional) - An object, whose keys are the names of the config profiles and its values are arrays of webpack plugins
 * * webpackConfig - (required) - An object of type webpack.Configuration
 * @param configFilePath (required) - The path to the config file to use and pass to webpack.
 * @param pluginsProfileName (optional) - If provided, adds the list of plugins under the corresponding key in the config to the plugin array. For example, "production" would add "productionPlugins" from the config to "plugins" in the config.
 * @returns A promise that resolves with true if the execution is successful.
 */
export async function watchFromFile(configFilePath: string, pluginsProfileName?: string): Promise<Server> {
	console.log('[ramster-webpack devserver info]: Starting script...')
	let configData = (await import(configFilePath)) as any
	console.log('[ramster-webpack devserver info]: Config loaded.')
	if (!configData.webpackConfig) {
		throw new CustomError('The provided file does not contain a webpackConfig object.')
	}
	if (pluginsProfileName && configData.profileWebpackPlugins) {
		configData.webpackConfig = injectPluginsInConfig(configData.webpackConfig, configData.profileWebpackPlugins, pluginsProfileName)
	}
	console.log('[ramster-webpack devserver info]: Starting devserver...')
	const devServer = await watch(configData.webpackConfig, configData.devserverConfig)
	console.log(`[ramster-webpack devserver info]: Devserver for config file ${configFilePath} started successfully.`)
	return devServer
}
