import {checkDevserver, closeDevserver} from '../testTools'
import * as path from 'path'
import {strict as assert} from 'assert'
import {watchFromFile} from '../../dist'

const
	basePath = path.join(__dirname, '../../test')

describe('watchFromFile', function() {
	it(`should throw an error with the correct message if a path to an invalid config file that's missing a webpackConfig object is provided`, function() {
		this.timeout(40000)
		return (async function() {
			let didThrowAnError = true,
				messageShouldBe = `The provided file does not contain a webpackConfig object.`,
				devServer = null
			try {
				devServer = await watchFromFile(path.join(basePath, 'invalidWebpackConfig.js'))
			} catch(e) {
				assert.strictEqual(e.name, 'CustomError', `bad value ${e.name} for e.name, expected CustomError`)
				assert.strictEqual(e.message, messageShouldBe, `bad value ${e.message} for e.message, expected ${messageShouldBe}`)
				didThrowAnError = true
			}
			if (devServer) {
				await closeDevserver(devServer)
			}
			assert.strictEqual(didThrowAnError, true, `bad value ${didThrowAnError} for didThrowAnError, expected true`)
		})()
	})
	it('should execute successfully and start a devserver for the bundle if a path to a valid config file is provided and no pluginsProfileName is provided', function() {
		this.timeout(40000)
		return (async function() {
			const devServer = await watchFromFile(path.join(__dirname, '../webpackConfig.js'))
			devServer.on(
				'error',
				(err) => {
					throw err
				}
			)
			let isRunning = await checkDevserver(19900)
			await closeDevserver(devServer)
			assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
		})()
	})
	it('should execute successfully, build the bundle and generate a main.js file if a path to a valid config file is provided and pluginsProfileName is provided', function() {
		this.timeout(40000)
		return (async function() {
			const devServer = await watchFromFile(path.join(__dirname, '../webpackConfig.js'), 'production')
			devServer.on(
				'error',
				(err) => {
					throw err
				}
			)
			let isRunning = await checkDevserver(19900)
			await closeDevserver(devServer)
			assert.strictEqual(isRunning, true, `bad value ${isRunning} for isRunning, expected true`)
		})()
	})

	// after(function() {
	// 	setTimeout(() => process.exit(0), 5000)
	// })
})
