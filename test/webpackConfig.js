const
	BellOnBundlerErrorPlugin = require('bell-on-bundler-error-plugin'),
	path = require('path'),
	ProgressBarPlugin = require('progress-bar-webpack-plugin'),
	webpack = require('webpack')

let includePath = path.join(__dirname, './src'),
	publicPath = path.join(__dirname, './dist'),
	include = [includePath]

module.exports = {
	devserverConfig: {hostName: '127.0.0.1', port: 9999},
	profileWebpackPlugins: {
		local: [
			new webpack.NamedModulesPlugin()
		],
		production: [
			new webpack.NoEmitOnErrorsPlugin(),
			new webpack.HashedModuleIdsPlugin(),
			new webpack.DefinePlugin({
				'process.env': {
					'NODE_ENV': JSON.stringify('production')
				}
			})
		]
	},
	webpackConfig: {
		devtool: 'source-map',
		entry: [
			path.join(includePath, 'index.ts')
		],
		output: {
			path: publicPath,
			filename: '[name].js',
			chunkFilename: '[id].chunk.js',
			publicPath: '/dist/'
		},
		resolve: {
			extensions: ['.ts', '.js'],
			modules: ['node_modules']
		},
		module: {
			rules: [
				{
					test: /\.ts$/,
					include,
					exclude: [],
					use: ['awesome-typescript-loader']
				}, {
					test: /\.json$/,
					include,
					exclude: [],
					use: ['json2-loader']
				}
			]
		},
		stats: 'verbose',
		plugins: [
			new BellOnBundlerErrorPlugin(),
			new ProgressBarPlugin({
				format: '  build [:bar] (:percent) - (:elapsed seconds)',
				clear: false,
				complete: '#',
				summary: 'true'
			})
		]
	}
}
